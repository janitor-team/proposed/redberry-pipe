/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe;

/**
 * Base interface for passive object consumer.
 *
 * @param <T> type of consumed objects
 * @author Bolotin Dmitriy (bolotin.dmitriy@gmail.com)
 */
public interface InputPort<T> {
    /**
     * Put an element into this input port.
     *
     * <p>Putting of {@code null} is allowed only once and this indicates that the port should be <b>closed</b>. No
     * invocations of this method allowed for <b>closed</b> input port.</p>
     *
     * @param object object to put into this input port, or {@code null} if port should be closed
     * @throws InterruptedException
     */
    void put(T object);
}

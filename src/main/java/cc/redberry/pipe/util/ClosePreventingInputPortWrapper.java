/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

import cc.redberry.pipe.InputPort;

/**
 * This class prevents putting null to the nested input port.
 *
 * <p>To close nested port use {@code close()} method.</p>
 *
 * @param <T> type of port
 */
public class ClosePreventingInputPortWrapper<T> implements InputPort<T> {
    private final InputPort<T> innerInputPort;

    public ClosePreventingInputPortWrapper(InputPort<T> innerInputPort) {
        if (innerInputPort == null)
            throw new NullPointerException();
        this.innerInputPort = innerInputPort;
    }

    @Override
    public void put(T object) {
        if (object != null)
            innerInputPort.put(object);
    }

    /**
     * Use this method to close (put null) inner port.
     *
     * @throws InterruptedException
     */
    public void close() throws InterruptedException {
        innerInputPort.put(null);
    }
}

/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import cc.redberry.pipe.OutputPortCloseable;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Counting wrapper for {@link OutputPort}.
 *
 * @author Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 */
public class CountingOutputPort<T> implements OutputPortCloseable<T> {
    private final OutputPort<T> innerOutputPort;
    private final AtomicBoolean closed = new AtomicBoolean(false);
    private final AtomicLong counter = new AtomicLong();

    public CountingOutputPort(OutputPort<T> innerOutputPort) {
        this.innerOutputPort = innerOutputPort;
    }

    @Override
    public T take() {
        if (closed.get())
            return null;
        T value = innerOutputPort.take();
        if (value != null)
            counter.incrementAndGet();
        else
            closed.set(true);
        return value;
    }

    /**
     * Returns the count of object passed through this port.
     */
    public long getCount() {
        return counter.get();
    }

    public boolean isClosed() {
        return closed.get();
    }

    @Override
    public String toString() {
        return Integer.toString(counter.intValue());
    }

    @Override
    public void close() {
        if (innerOutputPort instanceof OutputPortCloseable)
            ((OutputPortCloseable) innerOutputPort).close();
    }
}

package cc.redberry.pipe.blocks;

import java.util.Objects;

/**
 * Buffer status snapshot.
 */
public final class BufferStatus {
    /**
     * Is buffer closed (no more elements will be added by upstream process, it may still contain some elements to be
     * consumed by downstream process)
     */
    public final boolean closed;

    /**
     * Current buffer capacity (maximal number of objects it can hold)
     */
    public final int capacity;

    /**
     * Number of objects added to the buffer by upstream process(es)
     */
    public final long putCount;

    /**
     * Number of objects taken from the buffer bu downstream process(es)
     */
    public final long takeCount;

    /**
     * Value of System.currentTimeMillis() for the moment of object creation
     */
    public final long timestamp;

    public BufferStatus(boolean closed, int capacity, long putCount, long takeCount) {
        this(closed, capacity, putCount, takeCount, System.currentTimeMillis());
    }

    public BufferStatus(boolean closed, int capacity, long putCount, long takeCount, long timestamp) {
        if (putCount < takeCount)
            throw new IllegalArgumentException();
        if (putCount - takeCount > capacity)
            throw new IllegalArgumentException();
        this.closed = closed;
        this.capacity = capacity;
        this.putCount = putCount;
        this.takeCount = takeCount;
        this.timestamp = timestamp;
    }

    /**
     * Returns buffer size (current number of elements in the buffer).
     *
     * size = putCount - takeCount
     *
     * @return buffer size
     */
    public int size() {
        return (int) (putCount - takeCount);
    }

    /**
     * Returns buffer fullness: double value from 1.0 to 0.0, 1.0 - for completely full buffer, 0.0 for empty buffer.
     *
     * fullness = size() / capacity
     *
     * @return buffer fullness
     */
    public double fullness() {
        return 1.0 * size() / capacity;
    }

    /**
     * Returns true if this buffer is closed and contain no elements
     */
    public boolean isClosedAndEmpty() {
        return size() == 0 && closed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BufferStatus)) return false;
        BufferStatus that = (BufferStatus) o;
        return closed == that.closed &&
                capacity == that.capacity &&
                putCount == that.putCount &&
                takeCount == that.takeCount &&
                timestamp == that.timestamp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(closed, capacity, putCount, takeCount, timestamp);
    }

    @Override
    public String toString() {
        return "BufferStatus{" +
                "closed=" + closed +
                ", capacity=" + capacity +
                ", putCount=" + putCount +
                ", takeCount=" + takeCount +
                ", timestamp=" + timestamp +
                '}';
    }
}

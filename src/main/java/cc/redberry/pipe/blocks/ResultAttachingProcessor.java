package cc.redberry.pipe.blocks;

import cc.redberry.pipe.Processor;

/**
 * {@link cc.redberry.pipe.Processor} wrapper used to save input value passed to the wrapped processor and attache it to
 * the output.
 *
 * @author Bolotin Dmitriy
 * @see InputOutputPair
 */
public class ResultAttachingProcessor<InputT, OutputT> implements Processor<InputT, InputOutputPair<InputT, OutputT>> {
    private final Processor<InputT, OutputT> processor;

    /**
     * Use factory method.
     *
     * @param processor
     */
    public ResultAttachingProcessor(Processor<InputT, OutputT> processor) {
        this.processor = processor;
    }

    @Override
    public final InputOutputPair<InputT, OutputT> process(InputT input) {
        OutputT output = processor.process(input);
        return new InputOutputPair<InputT, OutputT>(input, output);
    }
}

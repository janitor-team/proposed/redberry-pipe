package cc.redberry.pipe.blocks;

import cc.redberry.pipe.Processor;
import cc.redberry.pipe.ProcessorFactory;

/**
 * {@link cc.redberry.pipe.ProcessorFactory} wrapper to produce {@link ResultAttachingProcessor}.
 */
public final class ResultAttachingProcessorFactory<InputT, OutputT> implements ProcessorFactory<InputT, InputOutputPair<InputT, OutputT>> {
    private final ProcessorFactory<InputT, OutputT> processorFactory;

    public ResultAttachingProcessorFactory(ProcessorFactory<InputT, OutputT> processorFactory) {
        this.processorFactory = processorFactory;
    }

    @Override
    public Processor<InputT, InputOutputPair<InputT, OutputT>> create() {
        return new ResultAttachingProcessor<>(processorFactory.create());
    }
}

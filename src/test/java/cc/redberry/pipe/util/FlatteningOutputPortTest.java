package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;

public class FlatteningOutputPortTest {
    @Test
    public void test1() throws Exception {
        List<String> strings0 = Arrays.asList(new String[]{"k1", "k2", "k3", "k4"});
        List<String> strings1 = Arrays.asList(new String[]{"a1", "a2", "a3"});
        List<String> strings2 = Arrays.asList(new String[]{"a1s", "as2", "sk3", "sk4", "a3s"});
        List<OutputPort<String>> ssList = (List) Arrays.asList(new OutputPort[]{
                new IteratorOutputPortAdapter<String>(strings0),
                new IteratorOutputPortAdapter<String>(strings1),
                new IteratorOutputPortAdapter<String>(strings2)});
        OutputPort<OutputPort<String>> opop = new IteratorOutputPortAdapter<>(ssList);

        OutputPort<String> sPort = new FlatteningOutputPort<String>(opop);
        for (String s : strings0)
            assertEquals(s, sPort.take());
        for (String s : strings1)
            assertEquals(s, sPort.take());
        for (String s : strings2)
            assertEquals(s, sPort.take());

        assertEquals(null, sPort.take());
    }
}
